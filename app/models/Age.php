<?php

class Age extends \Eloquent {
	protected $fillable = [];
    protected $table = "ages";

    /**
     * Making many to many Ages <-> Workshops
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workshops()
    {
        return $this->belongsToMany('Workshop', 'ages_workshops', 'ages_id', 'workshop_id');
    }
}