<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 *  HomeController Routes - Basic navigation
 */

Route::get('/', "HomeController@showWelcome");
Route::get('vragen', "HomeController@showQuestions");
Route::get('feest', "HomeController@showParty");
Route::get('feestje', "HomeController@showParty");
Route::get('vakantie', "HomeController@showVacation");
Route::get('schooljaar', "HomeController@showYear");
