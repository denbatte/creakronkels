@extends('layouts.default_large')

@section('content')

<article class="row">
    <div class="col-md-12 titel">Workshops in de kijker</div>

    @foreach($workshops as $workshop)
    <div class="workshop col-sm-3 col-sm-offset-1" style="background: url('img/techkronkel.jpg'); background-repeat: no-repeat;">
        <div class="workshop-title">{{ $workshop->name }}</div>
        <div class="workshop-data">3de, 4de, 5de en 6de leerjaar. <br />Van {{ strftime("%d", strtotime($workshop->start_at)) }} t.e.m. {{ strftime("%d", strtotime($workshop->end_at)) }} {{ strftime("%B", strtotime($workshop->start_at)) }}.</div>
        <div>
            <svg height="50" width="50" class="speach-corner">
                <polygon points="0,0 50,0 25,25" />
            </svg>
        </div>
        <span>
            <div class="btn-choice">
                <div class="btn btn-warning icon-square"><i class="glyphicon glyphicon-share-alt" style="font-size: 1.45em"></i></div>
                <div class="btn btn-warning">ONTDEKKEN&nbsp;</div><div class="cirkle">OF</div><div class="btn btn-success">INSCHRIJVEN</div>
            </div>
        </span>
    </div>
    @endforeach

</article>

<article class="row">
    <div class="titel col-sm-12">
        Nieuws
    </div>
    <div class="col-sm-12">
        @foreach ($news as $new)
        <div class="news-title">{{ $new->title }}</div>
        <div class="news-date">{{ strftime("%d/%m/%Y", strtotime($new->updated_at)) }}</div>
        <div class="news-content">
            {{ $new->body }}
        </div>
        @endforeach
    </div>
</article>

@stop