@extends('layouts.default_small')

@section('content')


<article class="row">
    <div class="col-sm-12">

        <div>
            <div style="float: left;">{{ HTML::image("img/filter.png", "filter", array("style" => "margin-right: 20px;")) }}</div>
            <div style="float: left; margin-top: 7px; margin-right: 5px; font-size: 1.2em;">Toon me</div>
            <div style="float: left; margin-right: 5px;">
                <select class="fancySelect">
                    <option value="">alle</option>
                    <option>Lorem Ipsum</option>
                    <option>Dolor Sit</option>
                    <option>Vehicula Ornare</option>
                </select>
            </div>
            <div style="float: left; margin-top: 7px; margin-right: 5px; font-size: 1.2em;">workshops voor</div>
            <div style="float: left; margin-right: 5px;">
                <select class="fancySelect">
                    <option value="">iedereen</option>
                    <option>Lorem Ipsum</option>
                    <option>Dolor Sit</option>
                    <option>Vehicula Ornare</option>
                </select>
            </div>
            <div style="float: left; margin-top: 7px; margin-right: 5px; font-size: 1.2em;">tijdens de</div>
            <div style="float: left; margin-right: 5px;">
                <select class="fancySelect">
                    <option value="">vakantie</option>
                    <option>Lorem Ipsum</option>
                    <option>Dolor Sit</option>
                    <option>Vehicula Ornare</option>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="titel">Workshops tijdens de vakantie.</div>

        @foreach($workshops as $workshop)
            <div style="border-left: 5px solid cornflowerblue; padding: 10px; margin-bottom: 10px;">
                <div style="float: left;">
                    <!--{{ HTML::image("img/workshops/".$workshop->image, $workshop->image . "-afbeelding", array("style" => "float:left; height:75px; margin-right: 20px;")) }}-->

                    <?php if(file_exists("img/workshops/".$workshop->image)) {
                        echo "<img class='' src='img/workshops/'.$workshop->image>";
                    } else  {
                        echo "<img src='img/creakronkels_logo.jpg' height='75'>";
                    };
                    ?>
                </div>
                <div>
                    <div class="workshoptitle">{{ $workshop->name }}</div>
                    <div>Van {{ strftime("%d", strtotime($workshop->start_at)) }} t.e.m. {{ strftime("%d", strtotime($workshop->end_at)) }} {{ strftime("%B", strtotime($workshop->start_at)) }}</div>
                    <div>Kleuters vanaf 2.5 jaar</div><br />
                    <div>{{ $workshop->short }}</div>
                </div>
            </div>
        @endforeach

    </div>
</article>

<script>
    $(".fancySelect").fancySelect();
</script>

@stop