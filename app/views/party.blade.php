@extends('layouts.default_small')

@section('content')
<article class="row">
    <div class="col-sm-12">
        <div class="titel">Een creatief feest.</div>
        <p>
            Wil je al deze creativiteit eens bij je thuis?<br />
            Tijdens een verjaardags- of een communiefeest?<br />
            Of gewoon zomaar?
        </p>
        <p>
            Sport en spel, stof, techniek of kunstwerken maken.
            Creakronkels pakt zijn materiaal in en komt naar je toe.<br />
            Verschillende workshops zijn mogelijk, afhankelijk van de leeftijd, het aantal deelnemers en de beschikbare ruimte!<br />
        </p>
        <p>
            Een activiteit duurt ongeveer 2 uur en op het einde kan iedereen iets mee naar huis nemen.<br />
        </p>
        <p>
            De meeste workshops kosten 12 euro/persoon met een minimum van 8 deelnemers.<br />
            Vanaf 20 kilometer vragen wij ook een kleine kilometervergoeding.
        </p>
        <p>
            Indien je interesse hebt, twijfel dan niet om <a href="vragen">contact met ons op te nemen</a>.

        </p>
    </div>
</article>


@stop