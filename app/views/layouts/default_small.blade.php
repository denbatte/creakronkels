﻿﻿<!DOCTYPE html>
<html>
<head>
    <title>Creakronkels - Creatieve workshops | Startpagina</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="description" content="Creakronkels zijn creatieve workshops gegeven op een leuke maar educatieve manier door ervaren leerkrachten.">

    <!-- Twitter bootstrap and custom styles -->
    {{ HTML::style("css/bootstrap.css") }}
    {{ HTML::style("css/style.css") }}
    {{ HTML::style("css/fancySelect.css") }}

    <!-- JQuery 1.11.1 minified -->
    {{ HTML::script("js/jq.js") }}
    {{ HTML::script("js/ga.js") }}
    {{ HTML::script("js/fancySelect.js") }}

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon/favicon.ico">
</head>

<body><a name="top" href="#"></a>
<div class="site-wrapper container">
    <header>
        <div class="row">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigatie</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="."><span class="header-title">Creakronkels</span></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <a href="schooljaar" class="navbar-link"><li class="menu-tab icon-graduation-cap" style="background-color: red"><br />Schooljaar</li></a>
                            <a href="vakantie" class="navbar-link"><li class="menu-tab icon-sun" style="background-color: darkorange"><br />Vakantie</li></a>
                            <a href="feest" class="navbar-link"><li class="menu-tab icon-flag" style="background-color: yellowgreen"><br />Feestjes</li></a>
                            <a href="vragen" class="navbar-link"><li class="menu-tab icon-chat" style="background-color: cornflowerblue"><br />Vragen?</li></a>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </header>

    @yield('content')

    </div>
    <footer>
        <div class="col-sm-4">
            <b style="color: yellowgreen;">Contactgegevens:</b><br />
            <i class="glyphicon glyphicon-envelope"></i> info@creakronkels.be<br />
            <br />
            facebook<br />
            Twitter
        </div>
        <div class="col-sm-4">
            <b style="color: yellowgreen;">Sitemap</b>
            <ul>
                <li>Wie zijn wij?</li>
                <li>Leerrijke workshops</li>
                <ul>
                    <li>Vakanties</li>
                    <li>Schooljaar 2014-2015</li>
                    <li>Feestjes, Verjaardag, Slaapfeestje</li>
                    <li>Scholen</li>
                </ul>
                <li>Thema's</li>
                <ul>
                    <li>Creativiteit</li>
                    <li>Beweging</li>
                    <li>Techniek</li>
                </ul>
                <li>Inschrijven</li>
                <li>Vragen en antwoorden</li>
                <li>Gebruikersovereenkomst</li>
            </ul>
        </div>
        <div class="col-sm-4">
            <b style="color: yellowgreen;">Cookie beleid.</b><br />
            Creakronkels.be maakt gebruik van (Google analythics) Cookies om uw gebruikerservaring te verbeteren.<br />
            Indien u niet wenst dat Creakronkels deze cookies plaatst op uw computer kan u deze hieronder de-activeren.<br />
            <div class="btn btn-info">de-activeer</div><br /><br />
            Meer details en informatie kan u vinden in onze <a href="gebruikersovereenkomst" class="text-info">gebruikersovereenkomst</a>.
            <br /><br />
            Copyright Creakronkels 2014
            <a href="#top"><div style="position: absolute; bottom: 0px; right: 0px;" class="text-info">Terug naar boven<i class="glyphicon glyphicon-arrow-up"></i>&nbsp;</div></a>
        </div>
    </footer>
<script>
    $('.flash-message').delay(5000).slideUp();
</script>
</body>
</html>