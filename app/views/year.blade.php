@extends('layouts.default_small')

@section('content')
<article class="row">
    <div class="col-sm-12">
        <div>
            <div style="float: left;">{{ HTML::image("img/filter.png", "filter", array("style" => "margin-right: 20px;")) }}</div>
            <div style="float: left; margin-top: 7px; margin-right: 5px; font-size: 1.2em;">Toon me</div>
            <div style="float: left; margin-right: 5px;">
                <select class="fancySelect">
                    <option value="">alle</option>
                    <option>Lorem Ipsum</option>
                    <option>Dolor Sit</option>
                    <option>Vehicula Ornare</option>
                </select>
            </div>
            <div style="float: left; margin-top: 7px; margin-right: 5px; font-size: 1.2em;">workshops voor</div>
            <div style="float: left; margin-right: 5px;">
                <select class="fancySelect">
                    <option value="">iedereen</option>
                    <option>Lorem Ipsum</option>
                    <option>Dolor Sit</option>
                    <option>Vehicula Ornare</option>
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="titel">Workshops tijdens het schooljaar.</div>
    </div>
</article>

<script>
    $(".fancySelect").fancySelect();
</script>
@stop