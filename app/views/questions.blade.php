@extends('layouts.default_small')

@section('content')
<article class="row">
    <div class="col-sm-8">
        <div>
            <div style="float: left;">{{ HTML::image("img/filter.png", "filter", array("style" => "margin-right: 20px;")) }}</div>
            <div style="float: left; margin-top: 7px; margin-right: 5px; font-size: 1.2em;">Ik ben op zoek naar een vraag in de categorie</div>
            <div style="float: left; margin-right: 5px;">
                <select class="fancySelect">
                    <option value="">alle</option>
                    <option>Lorem Ipsum</option>
                    <option>Dolor Sit</option>
                    <option>Vehicula Ornare</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="titel">Veel gestelde vragen...</div>
        <ul>
            @foreach($questions as $question)
            <li class="faq-a">
                <span  class="faq-q">V: {{ $question->question }}</span>
                A: {{ $question->answer }}
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col-sm-4">
        <div class="titel">Extra vragen?</div>
        <div>Stuur ons een berichtje, dan beantwoorden wij je vragen met veel plezier.</div>
        <br />
        <form>
            <div class="input-lg"><input type="text" name="name" placeholder="naam"></div>
            <div class="input-lg"><input type="text" name="email" placeholder="email"></div>
            <div class="input-lg"><input type="text" name="phone" placeholder="telefoon"></div>
            <div class="input-lg"><textarea>

                </textarea></div>
            <br /><br />
            <div class="btn btn-info">Stuur</div>
        </form>
    </div>
</article>

<script>
    $(".fancySelect").fancySelect();

    $(document).ready(function() {
        $(".fancySelect").change(function(){
            alert("lol");
    });

    })
</script>
@stop