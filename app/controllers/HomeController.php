<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
        $news = News::all();
        $workshops = Workshop::where("prior", "=", "1")->where("active", "=", "1")->limit(3)->get();
		return View::make('index')->with(array("news" => $news, "workshops" => $workshops));
	}

    public function showQuestions()
    {
        $questions = Question::all();
        return View::make('questions')->with(array("questions" => $questions));
    }

    public function showParty()
    {
        return View::make('party');
    }

    public function showVacation()
    {
        $workshops = Workshop::where('period', '!=', 'year')->where("active", "=", "1")->get();
        return View::make('vacation')->with(array("workshops" => $workshops));
    }

    public function showYear()
    {
        $workshops = Workshop::where('period', '=', 'year')->where("active", "=", "1")->get();
        return View::make('year');
    }

}
