<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkshopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workshops', function(Blueprint $table)
		{
			$table->increments('id');
            $table->boolean("active");
            $table->boolean("prior");
            $table->string("name");
            $table->text("short");
            $table->text("long");
            $table->string("image");
            $table->smallInteger("price");
            $table->string("place");
            $table->string("period");
            $table->timestamp("start_at");
            $table->timestamp("end_at");
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workshops');
	}

}
